def vowel_swapper(string):
    # ==============
    Dictionary = {'A': "4", 'E': "3", 'I': "!", 'O': "000", 'U': "|_|", 'a': "4", 'e': "3", 'i': "!", 'o': "ooo",
                  'u': "|_|"}
    string = "iamastringoe"

    def vowel_swapper(string, dict):
        for key in dict.keys():
            string = string.replace(key, str(dict[key]))
        return string

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console